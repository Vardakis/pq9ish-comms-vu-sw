/**
  ******************************************************************************
  * @file   fatfs.c
  * @brief  Code for fatfs applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

#include "fatfs.h"

uint8_t retSD;    /* Return value for SD */
char SDPath[4];   /* SD logical drive path */
FATFS SDFatFS;    /* File system object for SD logical drive */
FIL SDFile;       /* File object for SD */

/* USER CODE BEGIN Variables */
#include <stdio.h>
extern uint16_t station_id;
/* USER CODE END Variables */

void
MX_FATFS_Init(void)
{
    /*## FatFS: Link the SD driver ###########################*/
    retSD = FATFS_LinkDriver(&SD_Driver, SDPath);

    /* USER CODE BEGIN Init */
    if (retSD != 0) {
        Error_Handler();
    }

    if (f_mount(&SDFatFS, (TCHAR const *)SDPath, 1) != FR_OK) {
        return;
    }
//    Error_Handler();

    fileReadTest();
    retSD = f_mkdir("SYS");
    if (retSD != FR_OK && retSD != FR_EXIST) {
        Error_Handler();
    }

    retSD = f_mkdir("DATA");
    if (retSD != FR_OK && retSD != FR_EXIST) {
        Error_Handler();
    }
    /* USER CODE END Init */
}

/**
  * @brief  Gets Time from RTC
  * @param  None
  * @retval Time in DWORD
  */
DWORD
get_fattime(void)
{
    /* USER CODE BEGIN get_fattime */
    return 0;
    /* USER CODE END get_fattime */
}

/* USER CODE BEGIN Application */
void
fileWriteTest()
{
    int res;

    res = f_open(&SDFile, "DUMMY.TXT", FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK) {
        Error_Handler();
    }

    //write
    res = f_lseek(&SDFile, f_size(&SDFile));

    for (int tick = 0; tick < 10; tick++) {
        f_printf(&SDFile, "PQ9ISH-Tick: %d \n", tick);
    }

    f_close(&SDFile);
}

void
fileReadTest()
{
    int res;
    char line[20];
    res = f_open(&SDFile, "STATION.CFG", FA_READ);
    if (res != FR_OK) {
        return;
    }

    //read
    f_gets(line, sizeof line, &SDFile);
    sscanf(line, "%hu", &station_id);

    res = f_close(&SDFile);
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
