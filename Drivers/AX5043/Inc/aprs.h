/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APRS_H_
#define APRS_H_

#include "ax25.h"
#include "ax5043.h"
//#include "pq9ish.h"

#define APRS_LON_STR_LEN                (9)
#define APRS_LAT_STR_LEN                (8)

typedef enum
{
  APRS_WEATHER = 0
} aprs_type_t;

typedef struct
{
  aprs_type_t   type;
  ax25_conf_t   hax25;
  char          lon[APRS_LON_STR_LEN];
  char          lat[APRS_LAT_STR_LEN];
} aprs_conf_t;

typedef struct {
	uint16_t sequence;
	uint8_t aval1;
	uint8_t aval2;
	uint8_t aval3;
	uint8_t aval4;
	uint8_t aval5;
	uint8_t aval6;
	uint8_t aval7;
	uint8_t aval8;
	uint8_t dval;
} tx_data_t;

int
aprs_init(aprs_conf_t *conf,
          const uint8_t *dest_addr,
          uint8_t dest_ssid,
          const uint8_t *src_addr,
          uint8_t src_ssid,
          aprs_type_t type,
          const char *lat,
          const char *lon);

int aprs_tx_packet(aprs_conf_t *conf, ax5043_conf_t *hax5043, const tx_data_t *tx_data);
int aprs_fmt_tlm_msg(const tx_data_t *tx_data, char *outbuf);
int aprs_fmt_tlm_param_msg(const char *vname1, const char *vname2, const char *vname3, const char *vname4, const char *vname5, const char *bname1, const char *bname2, const char *bname3, const char *bname4, const char *bname5, const char *bname6, const char *bname7, const char *bname8, char *outbuf);
int aprs_fmt_tlm_units_msg(const char *unit1, const char *unit2, const char *unit3, const char *unit4, const char *unit5, const char *lbl1, const char *lbl2, const char *lbl3, const char *lbl4, const char *lbl5, const char *lbl6, const char *lbl7, const char *lbl8, char *outbuf);
int aprs_fmt_tlm_eqns_msg(const char *eq1a, const char *eq1b, const char *eq1c, const char *eq2a, const char *eq2b, const char *eq2c, const char *eq3a, const char *eq3b, const char *eq3c, const char *eq4a, const char *eq4b, const char *eq4c, const char *eq5a, const char *eq5b, const char *eq5c, char *outbuf);
int aprs_fmt_tlm_bitspn_msg(const char *bs1, const char *bs2, const char *bs3, const char *bs4, const char *bs5, const char *bs6, const char *bs7, const char *bs8, const char *proj_name, char *outbuf);
int aprs_fmt_status_msg(const char *message, char * outbuf);

#endif /* APRS_H_ */
