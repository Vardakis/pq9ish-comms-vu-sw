/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017,2018 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ax5043.h"
#include "ax25.h"
#include <string.h>

static uint8_t __tx_buf[MAX_FRAME_LEN];
static size_t __tx_buf_idx = 0;
static uint8_t __tx_fifo_chunk[AX5043_FIFO_MAX_SIZE];
static uint32_t __tx_remaining = 0;
static void (*__tx_complete_callback)(ax5043_conf_t *) = NULL;

/**
 * FIFO command for the preamble. The third byte corresponds the length of
 * the preamble and is set by the TX routine for every frame
 */
static uint8_t __preamble_cmd[4] =
      { AX5043_FIFO_REPEATDATA_CMD,
        AX5043_FIFO_PKTSTART | AX5043_FIFO_RAW | AX5043_FIFO_NOCRC,
        0,
        AX25_SYNC_FLAG };

/**
 * FIFO command for the postable. The third byte corresponds the length of
 * the postable and is set by the TX routine for every frame
 */
static uint8_t __postamble_cmd[4] =
  {
  AX5043_FIFO_REPEATDATA_CMD,
  AX5043_FIFO_PKTSTART | AX5043_FIFO_PKTEND | AX5043_FIFO_RAW
      | AX5043_FIFO_NOCRC, 0, AX25_SYNC_FLAG };


/**
 * Indicates if a TX is currently active
 */
static volatile uint8_t __tx_active = 0;

static ax5043_conf_t *__ax5043_conf = NULL;

static inline int
set_tx_black_magic_regs ();

/**
 * Checks if the AX5043 handler is valid
 * @param conf the AX5043 configuration handler pointer
 * @return 1 if it is valid 0 otherwise
 */
static uint8_t
is_ax5043_conf_valid (ax5043_conf_t *conf)
{
  if(!conf || !conf->read || !conf->write || !conf->f_xtal) {
    return 0;
  }
  return 1;
}


static inline int
tx_fifo_irq_enable(ax5043_conf_t *conf, uint8_t enable)
{
  int ret;
  uint16_t val =
      enable ?
          AX5043_IRQMFIFOTHRFREE | AX5043_IRQMRADIOCTRL : AX5043_IRQMRADIOCTRL;
  /* Enable FIFO IRQs */
  ret = ax5043_spi_write_16(conf, AX5043_REG_IRQMASK1, val);
  if(ret) {
    return ret;
  }
  return AX5043_OK;
}


/**
 * Resets the AX5043
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_reset(ax5043_conf_t *conf)
{
  int ret;
  uint8_t val;

  if(!is_ax5043_conf_valid(conf)) {
    return -AX5043_INVALID_PARAM;
  }

  conf->rf_init = 0;
  conf->spi_select(0);
  conf->delay_us(100);
  conf->spi_select(1);
  conf->delay_us(100);
  conf->spi_select(0);
  conf->delay_us(100);

  /* Reset the chip using the appropriate register */
  val = BIT(7);
  ret = ax5043_spi_write_8(conf, AX5043_REG_PWRMODE, val);
  if(ret) {
    return ret;
  }
  conf->delay_us(100);
  /* Clear the reset bit, but keep REFEN and XOEN */
  ret = ax5043_spi_read_8(conf, &val, AX5043_REG_PWRMODE);
  if(ret) {
    return ret;
  }
  val &= (BIT(6) | BIT(5));
  ret = ax5043_spi_write_8(conf, AX5043_REG_PWRMODE, val);
  if(ret) {
    return ret;
  }
  conf->delay_us(100);

  ret = ax5043_set_power_mode(conf, POWERDOWN);
  if(ret) {
    return ret;
  }
  return AX5043_OK;
}

/**
 * Initialization routine for the AX5043 IC
 * @param conf the AX5043 configuration handler
 * @param spi the SPI handler
 * @param f_xtal the frequency of the crystal or the TCXO
 * @param vco the VCO mode
 * @param tx_complete_callback function to be called when a frame has been
 * transmitted. If none, set it to NULL
 * @return 0 on success or appropriate negative error code
 */
ax5043_code_t
ax5043_init (ax5043_conf_t *conf, uint32_t f_xtal,
             vco_mode_t vco,
             void (*tx_complete_callback)(ax5043_conf_t *))
{
  int ret;
  uint8_t revision;
  uint8_t val;

  if(!conf || !conf->read || !conf->write) {
    return -AX5043_INVALID_PARAM;
  }

  /* Set the initial parameters */
  //TODO: verify that omitting memset does not break anything
//  memset(conf, 0, sizeof(ax5043_conf_t));

  switch (vco) {
    case VCO_INTERNAL:
    case VCO_EXTERNAL:
      conf->vco = vco;
    break;
    default:
      return -AX5043_INVALID_PARAM;
  }

  conf->rf_init = 0;
  conf->freqsel = -1;
  conf->f_xtal = f_xtal;
  if(conf->f_xtal > 24800000) {
    conf->f_xtaldiv = 2;
  }
  else {
    conf->f_xtaldiv = 1;
  }

  /* Try first to read the revision register of the AX5043 */
  ret = ax5043_spi_read_8(conf, &revision, AX5043_REG_REV);
  if(ret) {
    return ret;
  }

  if(revision != AX5043_REV) {
    return -AX5043_NOT_FOUND;
  }

  /* To ensure communication try to write and read the scratch register */
  val = AX5043_SCRATCH_TEST;
  ret = ax5043_spi_write_8(conf, AX5043_REG_SCRATCH, val);
  if (ret) {
    return ret;
  }

  val = 0x0;
  ret = ax5043_spi_read_8(conf, &val, AX5043_REG_SCRATCH);
  if(ret) {
    return ret;
  }

  if(val != AX5043_SCRATCH_TEST) {
    return -AX5043_NOT_FOUND;
  }

  ret = ax5043_reset(conf);
  if(ret) {
    return ret;
  }

  ret = ax5043_set_pll_params (conf);
  if(ret) {
    return ret;
  }

  /* Write the performance register F35 based on the XTAL frequency */
  if(conf->f_xtaldiv == 1) {
    ret = ax5043_spi_write_8(conf, 0xF35, 0x10);
  }
  else{
    ret = ax5043_spi_write_8(conf, 0xF35, 0x11);
  }
  if(ret) {
    return ret;
  }

  /* FIFO maximum chunk */
  ret = ax5043_spi_write_8(conf, AX5043_REG_PKTCHUNKSIZE,
                           AX5043_PKTCHUNKSIZE_240);
  if(ret) {
    return ret;
  }

  /* Set RF parameters */
  ret = ax5043_freqsel(conf, FREQA_MODE);
  if(ret) {
    return ret;
  }

  /*
   * We use APRS for all transmitted frames. APRS is encapsulated in a
   * AX.25 frame. For 9600 baudrate is FSK9600 G3RUH compatible modem
   */
  ret = ax5043_aprs_framing_setup(conf);
  if(ret) {
    return ret;
  }

  /* Setup TX only related parameters */
  ret = ax5043_conf_tx_path (conf);
  if(ret) {
    return ret;
  }

  /* Set an internal copy for the IRQ handler */
  __ax5043_conf = conf;

  /* Set the TX complete callback internal copy */
  __tx_complete_callback = tx_complete_callback;

  /*
   * Set the FIFO IRQ threshold. During TX, when the free space is larger
   * than this threshold, an IRQ is raised
   */
  ret = ax5043_spi_write_16(conf, AX5043_REG_FIFOTHRESH1, AX5043_FIFO_FREE_THR);
  ret = ax5043_spi_write_16(conf, AX5043_REG_IRQMASK1, AX5043_IRQMRADIOCTRL);
  ret = ax5043_spi_write_8(conf, AX5043_REG_RADIOEVENTMASK0, AX5043_REVMDONE);
  if(ret) {
    return ret;
  }
  return AX5043_OK;
}


/**
 * Performs TX specific configuration of the AX5043
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_conf_tx_path (ax5043_conf_t *conf)
{
  int ret;

  ret = ax5043_set_tx_synth (conf);
  if(ret) {
    return ret;
  }

  ret = ax5043_set_tx_baud(conf, TX_BAUDRATE);
  if(ret) {
    return ret;
  }

  ret = ax5043_set_tx_freq(conf, TX_FREQ_HZ);
  if(ret) {
    return ret;
  }

  /* Our TX is on single ended mode */
  ret = ax5043_spi_write_8(conf, AX5043_REG_MODCFGA,
                           AX5043_TX_SINGLE_ENDED);
  if(ret) {
    return ret;
  }

  /* Set the rest of the performance registers for TX */
  ret = set_tx_black_magic_regs (conf);
  if(ret) {
    return ret;
  }

  /*
   * As our board has an external PA, reduce the output power to reduce
   * the excess bandwidth emissions
   */
  ret = ax5043_spi_write_16 (conf, AX5043_REG_TXPWRCOEFFB1, AX5043_OUTPUT_POWER);
  if (ret) {
    return ret;
  }

  return AX5043_OK;
}

/**
 * Sets the power mode of the AX5043
 * @param conf the AX5043 configuration handler
 * @param mode the power mode
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_power_mode (ax5043_conf_t *conf, power_mode_t mode)
{
  int ret;
  uint8_t val;

  if (!is_ax5043_conf_valid (conf)) {
    return -AX5043_INVALID_PARAM;
  }

  /* Read the contents of the register */
  ret = ax5043_spi_read_8(conf, &val, AX5043_REG_PWRMODE);
  if(ret) {
    return ret;
  }

  /* Keep REFEN and XOEN values */
  val &= (BIT(6) | BIT(5));

  switch(mode) {
    case POWERDOWN:
      val |= AX5043_POWERDOWN;
      break;
    case DEEPSLEEP:
      val |= AX5043_DEEPSLEEP;
      break;
    case STANDBY:
      val |= AX5043_STANDBY;
      break;
    case FIFO_ENABLED:
      val |= AX5043_FIFO_ENABLED;
      break;
    case RECEIVE_MODE:
      val |= AX5043_RECEIVE_MODE;
      break;
    case RECEIVER_RUNNING:
      val |= AX5043_RECEIVER_RUNNING;
      break;
    case RECEIVER_WOR:
      val |= AX5043_RECEIVER_WOR;
      break;
    case TRANSMIT_MODE:
      val |= AX5043_TRANSMIT_MODE;
      break;
    case FULLTX:
      val |= AX5043_FULLTX;
      break;
    default:
      return -AX5043_INVALID_PARAM;
  }
  return ax5043_spi_write_8(conf, AX5043_REG_PWRMODE, val);
}

/**
 * Sets the RF frequency of the TX. If the previous TX frequency is
 * further enough than the new one, this function performs automatically
 * auto-ranging.
 *
 * @param conf the AX5043 configuration handler
 * @param freq the target RF frequency
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_tx_freq(ax5043_conf_t *conf, uint32_t freq)
{
  int ret;
  uint32_t prev_freq;
  uint32_t reg_val;
  uint8_t rfdiv = 0;
  uint8_t pllcodediv = 0;

  if(!is_ax5043_conf_valid(conf)) {
    return -AX5043_INVALID_PARAM;
  }

  /* Check the frequency range. The actual range depends on the VCO used */
  switch (conf->vco){
    case VCO_INTERNAL:
      if (freq >= MIN_RF_FREQ_INT_VCO_RFDIV0
          && freq <= MAX_RF_FREQ_INT_VCO_RFDIV0) {
        rfdiv = AX5043_RFDIV0;
      }
      else if (freq >= MIN_RF_FREQ_INT_VCO_RFDIV1
          && freq <= MAX_RF_FREQ_INT_VCO_RFDIV1) {
        rfdiv = AX5043_RFDIV1;
      }
      else {
        return -AX5043_INVALID_PARAM;
      }
      break;
    case VCO_EXTERNAL:
      if (freq >= MIN_RF_FREQ_EXT_VCO_RFDIV0
          && freq <= MAX_RF_FREQ_EXT_VCO_RFDIV0) {
        rfdiv = AX5043_RFDIV0;
      }
      else if (freq >= MIN_RF_FREQ_EXT_VCO_RFDIV1
          && freq <= MAX_RF_FREQ_EXT_VCO_RFDIV1) {
        rfdiv = AX5043_RFDIV1;
      }
      else {
        return -AX5043_INVALID_PARAM;
      }
      break;
    default:
      return -AX5043_INVALID_PARAM;
  }
  prev_freq = conf->tx_freq;
  pllcodediv = rfdiv | (conf->vco << 4);
  ret = ax5043_spi_write_8(conf, AX5043_REG_PLLVCODIV, pllcodediv);
  if(ret) {
    return ret;
  }

  /* Write properly the F34 performance register based on the RFDIV*/
  if(rfdiv == AX5043_RFDIV1) {
    ret = ax5043_spi_write_8(conf, 0xF34, 0x28);
  }
  else{
    ret = ax5043_spi_write_8(conf, 0xF34, 0x08);
  }
  if(ret) {
    return ret;
  }

  /*
   * Set the RF frequency
   * Frequency should be avoided to be a multiple integer of the crystal
   * frequency, so we always set to 1 the LSB
   */
  reg_val = ((uint32_t) (((float) freq / (float) conf->f_xtal) * (1 << 24))
      | 0x1);
  if(conf->freqsel == FREQA_MODE) {
    ret = ax5043_spi_write_32(conf, AX5043_REG_FREQA3, reg_val);
  }
  else {
    ret = ax5043_spi_write_32(conf, AX5043_REG_FREQB3, reg_val);
  }
  if(ret) {
    return ret;
  }

  /* Considered that the frequency successfully changed */
  conf->tx_freq = freq;

  /* If the frequency difference is great enough perform autoranging */
  if(freq + 25000000 > prev_freq || freq - 25000000 < prev_freq) {
    ax5043_autoranging(conf);
  }

  return AX5043_OK;
}

/**
 * Set the TX baudrate
 * @param conf the AX5043 configuration handler
 * @param baud the baudrate
 * @return 0 on success or negative error code
 */
int
ax5043_set_tx_baud(ax5043_conf_t *conf, uint32_t baud)
{
  int ret = AX5043_OK;
  uint32_t val;
  if(!is_ax5043_conf_valid(conf)) {
    return -AX5043_INVALID_PARAM;
  }

  val = (uint32_t)((((float) baud) / (float) conf->f_xtal) * (1 << 24)) | 0x1;
  ret = ax5043_spi_write_24(conf, AX5043_REG_TXRATE2, val);
  if(ret) {
    return ret;
  }

  conf->tx_baudrate = baud;

  /* Set also the deviation. We use modulation index h=1 */
  val = (uint32_t) ((((float) baud / 2.0) / (float) conf->f_xtal) * (1 << 24))
      | 0x1;
  ret = ax5043_spi_write_24(conf, AX5043_REG_FSKDEV2, val);
  if(ret) {
    return ret;
  }
  return AX5043_OK;
}

/**
 * Sets the currently used frequency registers (A or B)
 * @param conf the AX5043 configuration handler
 * @param f the frequency mode (A or B)
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_freqsel(ax5043_conf_t *conf, freq_mode_t f)
{
  if(!is_ax5043_conf_valid (conf) ) {
    return -AX5043_INVALID_PARAM;
  }

  if(f != FREQA_MODE && f != FREQB_MODE) {
    return -AX5043_INVALID_PARAM;
  }
  conf->freqsel = f;
  return AX5043_OK;
}

/**
 * Sets the TX frequency synthesizer related configuration registers.
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_tx_synth (ax5043_conf_t *conf)
{
  int ret;
  uint8_t val;
  if (!is_ax5043_conf_valid (conf)) {
    return -AX5043_INVALID_PARAM;
  }

  switch(conf->freqsel){
    case FREQA_MODE:
      val = 0x0;
      break;
    case FREQB_MODE:
      val = 1 << 7;
      break;
    default:
      return -AX5043_INVALID_PARAM;
  }

  /* Bypass external filter and use 100 kHZ loop bandwidth */
  val |= BIT(3) | BIT(0);
  ret = ax5043_spi_write_8(conf, AX5043_REG_PLLLOOP, val);
  if(ret) {
    return ret;
  }

  /*
   * Set the charge pump current based on the loop bandwidth
   * 68 uA @ 100 kHZ
   */
  ret = ax5043_spi_write_8(conf, AX5043_REG_PLLCPI, (uint8_t)(68 / 8.5));
  if(ret) {
    return ret;
  }
  ret = ax5043_spi_write_8(conf, AX5043_REG_XTALCAP, 0);
  return ret;
}

/**
 * Sets the PLL related configuration registers.
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_pll_params (ax5043_conf_t *conf)
{
  int ret;
  uint8_t i = 8;
  if(!is_ax5043_conf_valid (conf) ) {
    return -AX5043_INVALID_PARAM;
  }

  /* Set VCO to manual */
  ret = ax5043_spi_write_8(conf, AX5043_REG_PLLVCOI,
                           AX5043_PLLVCOI_MANUAL | (1250 / 50));
  if(ret) {
    return ret;
  }

  /*
   * According to the manual PLL ranging clock should be less than 1/10
   * of the PLL loop bandwidth. The smallest PLL bandwidth configuration
   * is 100 kHz.
   */
  while( conf->f_xtal / (1 << i) > 10000 ) {
    i++;
  }
  i = i > 15 ? 15 : i;
  ret = ax5043_spi_write_8(conf, AX5043_REG_PLLRNGCLK, i - 8);
  return ret;
}

/**
 * Performs auto-ranging using the frequency registers configured by
 * ax5043_freqsel().
 *
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_autoranging(ax5043_conf_t *conf)
{
  int ret = AX5043_OK;
  uint16_t pllranging_reg;
  uint8_t val = 0;

  if(!is_ax5043_conf_valid (conf) ) {
    return -AX5043_INVALID_PARAM;
  }

  switch(conf->freqsel){
    case FREQA_MODE:
      pllranging_reg = AX5043_REG_PLLRANGINGA;
      break;
    case FREQB_MODE:
      pllranging_reg = AX5043_REG_PLLRANGINGB;
      break;
    default:
      return -AX5043_INVALID_PARAM;
  }

  /* Write the initial VCO setting and start autoranging */
  val = BIT(4) | AX5043_VCOR_INIT;
  ret = ax5043_spi_write_8(conf, pllranging_reg, val);
  if(ret) {
    return ret;
  }

  conf->delay_us(10);
  val = 0;
  /* Wait until the autoranging is complete */
  while((val & BIT(4)) == 0) {
    ret = ax5043_spi_read_8(conf, &val, pllranging_reg);
    if(ret) {
      return ret;
    }
  }

  if(val & BIT(5)) {
    return -AX5043_AUTORANGING_ERROR;
  }

  return AX5043_OK;
}


/**
 *
 * @param conf the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_aprs_framing_setup (ax5043_conf_t *conf)
{
  int ret = AX5043_OK;
  uint8_t val = 0;

  if (!is_ax5043_conf_valid (conf)) {
    return -AX5043_INVALID_PARAM;
  }

  /* Set modulation */
  val = AX5043_MODULATION_FSK;
  ret = ax5043_spi_write_8(conf, AX5043_REG_MODULATION, val);
  if(ret) {
    return ret;
  }

  /*
   * As we do not use any external filter, try to filter from
   * the AX5043 the signal
   */
  ret = ax5043_spi_write_8(conf, AX5043_REG_MODCFGF,
                           AX5043_FREQSHAPE_GAUSSIAN_BT_05);
  if (ret) {
    return ret;
  }

  /* Set HDLC encoding: Differential = 1, Inverse = 1, Scrambling = 1 */
  ax5043_spi_write_8(conf, AX5043_REG_ENCODING,
                     AX5043_ENC_DIFF | AX5043_ENC_INV | AX5043_ENC_SCRAM);

  /* HDLC framing */
  ax5043_spi_write_8(conf, AX5043_REG_FRAMING,
                     AX5043_HDLC_FRAMING | AX5043_CRC16_CCITT);
  return ret;
}


static int
__tx_frame_end (ax5043_conf_t *conf)
{
  int ret;

  tx_fifo_irq_enable (conf, 0);

  /* Set AX5043 to power down mode */
  ret = ax5043_set_power_mode(conf, POWERDOWN);
  if(__tx_complete_callback) {
    __tx_complete_callback(conf);
  }
  __tx_active = 0;
  return ret;
}

static int
__tx_frame (ax5043_conf_t *conf, const uint8_t *in, uint32_t len,
            uint8_t preamble_len,
            uint8_t postamble_len,
            uint32_t timeout_ms)
{
  int ret = AX5043_OK;
  uint8_t single_fifo_access = 0;
  uint8_t data_cmd[3] = { AX5043_FIFO_VARIABLE_DATA_CMD, 0, 0};
  size_t chunk_size = 0;
  size_t avail;
  uint8_t val;
  uint32_t start = millis();

  /*
   * Apply preamble and postamble repetition length. Rest of the fields should
   * remain unaltered
   */
  __preamble_cmd[2] = preamble_len;
  __postamble_cmd[2] = postamble_len;

  memcpy(__tx_fifo_chunk, __preamble_cmd, sizeof(__preamble_cmd));
  chunk_size = sizeof(__preamble_cmd);
  __tx_buf_idx = 0;

  /*
   * Always leave some space for the postamble. This greatly reduces the
   * complexity of dealing with some corner cases
   */
  avail = min_ul (
      AX5043_FIFO_MAX_SIZE - sizeof(__preamble_cmd) - sizeof(data_cmd)
          - sizeof(__postamble_cmd), len);
  if(len == avail) {
    data_cmd[1] = len + 1;
    data_cmd[2] = AX5043_FIFO_PKTEND;
    __tx_remaining = 0;
    memcpy(__tx_fifo_chunk + chunk_size, data_cmd, sizeof(data_cmd));
    chunk_size += sizeof(data_cmd);
    memcpy(__tx_fifo_chunk + chunk_size, in, len);
    chunk_size += len;
    /*
     * At this point we are sure that the whole frame + postamble can fit in
     * the FIFO chunk
     */
    memcpy(__tx_fifo_chunk + chunk_size, __postamble_cmd,
           sizeof(__postamble_cmd));
    chunk_size += sizeof(__postamble_cmd);
    single_fifo_access = 1;
  }
  else {
    data_cmd[1] = avail + 1;
    data_cmd[2] = 0;
    memcpy(__tx_fifo_chunk + chunk_size, data_cmd, sizeof(data_cmd));
    chunk_size += sizeof(data_cmd);
    memcpy(__tx_fifo_chunk + chunk_size, in, avail);
    chunk_size += avail;

    memcpy(__tx_buf, in + avail, len - avail);
    __tx_remaining = len - avail;
    single_fifo_access = 0;
  }

  /* Set AX5043 to FULLTX mode */
  ret = ax5043_set_power_mode(conf, FULLTX);
  if(ret) {
    return ret;
  }

  ax5043_spi_wait_xtal (conf, 100);

  /* Wait for the FIFO to become ready */
  val = 0;
  while (!val) {
    ax5043_spi_read_8(conf, &val, AX5043_REG_POWSTAT);
    /* Select only the modem power state */
    val &= AX5043_SVMODEM;
    if (millis () - start > timeout_ms) {
      ret = -AX5043_TIMEOUT;
      break;
    }
  }

  /* Fire-up the first data to the FIFO */
  ret = conf->write(__tx_fifo_chunk, AX5043_REG_FIFODATA, chunk_size);
  if(ret) {
    return ret;
  }
  __tx_active = 1;
  /* Commit to FIFO ! */
  ret = ax5043_spi_write_8(conf, AX5043_REG_FIFOSTAT, AX5043_FIFO_COMMIT_CMD);
  /*
   * Enable FIFO free IRQ, only if it is needed.
   * From now on, the IRQ handler will deal with filling the FIFO properly if
   * the frame size exceeds the maximum FIFO packet size
   */
  if (!single_fifo_access) {
    tx_fifo_irq_enable(conf, 1);
  }
  else{
    tx_fifo_irq_enable(conf, 0);
  }
  return ret;
}

int
ax5043_tx_frame (ax5043_conf_t *conf, const uint8_t *in, uint32_t len,
                 uint8_t preamble_len,
                 uint8_t postamble_len,
                 uint32_t timeout_ms)
{
  int ret = 0;

  /* Wait for the previous frame to be transmitted */
  while(__tx_active) {
    ret++;
  }

  ret = __tx_frame(conf, in, len, preamble_len, postamble_len, timeout_ms);
  return ret;
}

/**
 * Wait the crystal to become ready
 * @param conf the AX5043 configuration handler
 * @param timeout_ms the timeout in milliseconds
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_spi_wait_xtal(ax5043_conf_t *conf, uint32_t timeout_ms)
{
  int ret;
  uint8_t val = 0x0;
  uint32_t start = millis();

  while(!val) {
    ret = ax5043_spi_read_8(conf,  &val, AX5043_REG_XTALSTATUS);
    if(ret) {
      return ret;
    }
    if((millis() - start) > timeout_ms) {
      return -AX5043_TIMEOUT;
    }
  }
  return 0;
}



ax5043_code_t
ax5043_spi_read_8(ax5043_conf_t *conf, uint8_t *out, uint16_t reg)
{
  int ret;
  ret = conf->read(out, reg, 1);
  return ret;
}

int
ax5043_spi_read_16(ax5043_conf_t *conf, uint16_t *out, uint16_t reg)
{
  int ret;
  uint8_t regs[2];
  uint16_t res;
  ret = conf->read(regs, reg, sizeof(uint16_t));
  res = regs[0];
  res = (res << 8) | regs[1];
  *out = res;
  return ret;
}

int
ax5043_spi_read_32(ax5043_conf_t *conf, uint32_t *out, uint16_t reg)
{
  int ret;
  uint8_t regs[4];
  uint32_t res;
  ret = conf->read(regs, reg, sizeof(uint32_t));
  res = regs[0];
  res = (res << 8) | regs[1];
  res = (res << 8) | regs[2];
  res = (res << 8) | regs[3];
  *out = res;
  return ret;
}

int
ax5043_spi_write_8(ax5043_conf_t *conf, uint16_t reg,
                    uint8_t in)
{
  return conf->write(&in, reg, 1);
}

int
ax5043_spi_write_16(ax5043_conf_t *conf, uint16_t reg,
                    uint16_t in)
{
  uint8_t data[2];
  data[0] = (in >> 8) & 0xFF;
  data[1] = in & 0xFF;
  return conf->write(data, reg, 2);
}

int
ax5043_spi_write_24(ax5043_conf_t *conf, uint16_t reg,
                    uint32_t in)
{
  uint8_t data[3];
  data[0] = (in >> 16) & 0xFF;
  data[1] = (in >> 8) & 0xFF;
  data[2] = in & 0xFF;
  return conf->write(data, reg, 3);
}

int
ax5043_spi_write_32(ax5043_conf_t *conf, uint16_t reg,
                    uint32_t in)
{
  uint8_t data[4];
  data[0] = (in >> 24) & 0xFF;
  data[1] = (in >> 16) & 0xFF;
  data[2] = (in >> 8) & 0xFF;
  data[3] = in & 0xFF;
  return conf->write(data, reg, 4);
}

/**
 * Sets properly some undocumented TX registers
 * @param conf  the AX5043 configuration handler
 * @return 0 on success or appropriate negative error code
 */
static inline int
set_tx_black_magic_regs (ax5043_conf_t *conf)
{
  int ret;
  ret = ax5043_spi_write_8 (conf, 0xF00, 0x0F);
  if (ret) {
    return ret;
  }

  ret = ax5043_spi_write_8 (conf, 0xF0C, 0x0);
  if (ret) {
    return ret;
  }

  /*FIXME: This assumes that we always use a TCXO */
  ret = ax5043_spi_write_8 (conf, 0xF11, 0x0);
  if (ret) {
    return ret;
  }

  ret = ax5043_spi_write_8 (conf, 0xF1C, 0x07);
  if (ret) {
    return ret;
  }

  ret = ax5043_spi_write_8 (conf, 0xF44, 0x24);
  if (ret) {
    return ret;
  }

  /* Dafuq? Got it from RadioLab */
  ret = ax5043_spi_write_8 (conf, 0xF18, 0x06);
  return ret;
}

/**
 * Enables/Disables the power amplifier pin
 * @param conf the AX5043 configuration handler
 * @param enable 1 to enable 0 to disable
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_enable_pwramp (ax5043_conf_t *conf, uint8_t enable)
{
  int ret;
  ret = ax5043_spi_write_8(conf, AX5043_REG_PWRAMP, enable & 0x1);
  return ret;
}

/**
 * Controls the ANTSEL pin
 * @param conf the AX5043 configuration handler
 * @param weak_pullup set to 1 to enable the weak pullup.
 * @param invert set to 1 to revert the ANTSEL logic
 * @param pfantsel use the pfantsel_t enumeration to set properly the behavior
 * of the ANTSEL pin
 * @return 0 on success or appropriate negative error code
 */
int
ax5043_set_antsel (ax5043_conf_t *conf, uint8_t weak_pullup,
                   uint8_t invert, pfantsel_t pfantsel)
{
  uint8_t b = 0x0;
  switch(pfantsel){
    case ANTSEL_OUTPUT_0:
    case ANTSEL_OUTPUT_1:
    case ANTSEL_OUTPUT_BB_TUBE_CLK:
    case ANTSEL_OUTPUT_DAC:
    case ANTSEL_OUTPUT_DIVERSITY:
    case ANTSEL_OUTPUT_EXT_TCXO_EN:
    case ANTSEL_OUTPUT_TEST_OBS:
    case ANTSEL_OUTPUT_Z:
      b = pfantsel;
      b |= (invert & 0x1) << 6;
      b |= (weak_pullup & 0x1) << 7;
      break;
    default:
      return -AX5043_INVALID_PARAM;
  }
  return ax5043_spi_write_8(conf, AX5043_REG_PINFUNCANTSEL, b);
}


/**
 * The IRQ handler for the AX5043
 * @return 0 on success, or appropriate negative error code
 */
int
ax5043_irq_callback ()
{
  int ret;
  uint8_t data_cmd[3] = { AX5043_FIFO_VARIABLE_DATA_CMD, 0, 0};
  size_t avail;
  size_t chunk_size;
  uint32_t tmp;
  uint16_t reg0 = 0x0;
  uint16_t reg1 = 0x0;

  if(!__ax5043_conf) {
    return AX5043_OK;
  }

  /* Read both IRQREQUEST1 & RADIOEVENTREQ1 at once, to save some cycles */
  ret = ax5043_spi_read_32(__ax5043_conf, &tmp, AX5043_REG_IRQREQUEST1);
  if(ret) {
    return ret;
  }
  reg0 = tmp >> 16;
  reg1 = tmp & 0xFFFF;


  /* TX is done! */
  if(reg1 & AX5043_REVMDONE) {
    __tx_frame_end(__ax5043_conf);
    return AX5043_OK;
  }

  /* If FIFO has free space fill in data */
  if(reg0 & AX5043_IRQRFIFOTHRFREE) {
    /* Always left some space for the postamble for a simplified logic */
    avail = min_ul (
        AX5043_FIFO_FREE_THR - sizeof(data_cmd) - sizeof(__postamble_cmd),
        __tx_remaining);
    data_cmd[1] = avail + 1;
    chunk_size = sizeof(data_cmd) + avail;
    memcpy(__tx_fifo_chunk + sizeof(data_cmd), __tx_buf + __tx_buf_idx, avail);

    if(avail == __tx_remaining) {
      data_cmd[2] = AX5043_FIFO_PKTEND;
      memcpy(__tx_fifo_chunk + chunk_size,
             __postamble_cmd, sizeof(__postamble_cmd));
      chunk_size += sizeof(__postamble_cmd);
      /* Mask the FIFO free IRQ as it is not needed anymore */
      tx_fifo_irq_enable(__ax5043_conf, 0);
    }
     memcpy(__tx_fifo_chunk, data_cmd, sizeof(data_cmd));
     __ax5043_conf->write(__tx_fifo_chunk, AX5043_REG_FIFODATA,
                      chunk_size);
    /* Commit to FIFO ! */
    ret = ax5043_spi_write_8(__ax5043_conf, AX5043_REG_FIFOSTAT,
                             AX5043_FIFO_COMMIT_CMD);

    __tx_remaining -= avail;
    __tx_buf_idx += avail;
  }
  return AX5043_OK;
}

